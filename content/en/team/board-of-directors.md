---
title: Board of Directors
description: Grey Software's Board of Director
category: Team
position: 5
boardOfDirectors:
  - name: Arsala
    avatar: https://gitlab.com/uploads/-/system/user/avatar/2274539/avatar.png
    position: President
    github: https://github.com/ArsalaBangash
    gitlab: https://gitlab.com/ArsalaBangash
    linkedin: https://linkedin.com/in/ArsalaBangash
---

## Board of Directors

<team-profiles :profiles="boardOfDirectors"></team-profiles>